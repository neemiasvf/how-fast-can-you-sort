/*------------------------------------------------------------------
 Priority Queue Node.h
 How Fast Can You Sort?

 Definition of the priority queue node as a class.
 Each node has only a value, which represents its priority.

 Created by Neemias Freitas on 11/24/14.
 Copyright (c) 2014 Neemias Freitas. All rights reserved.
 ------------------------------------------------------------------*/

#ifndef __How_Fast_Can_You_Sort___Priority_Queue_Node__
#define __How_Fast_Can_You_Sort___Priority_Queue_Node__
#include <iostream>


class PQNode {

public:

	// Manager Methods (constructors & destructors)
	/*------------------------------------------------------------------
	 An optimized constructor for the priority queue node.
	 It takes one argument (value), which will set the 'value' variable.
	 ------------------------------------------------------------------*/
	PQNode(int value);

	/*------------------------------------------------------------------
	 An optimized destructor for the priority queue node.
	 It sets NULL to the 'value' variable.
	 ------------------------------------------------------------------*/
	~PQNode(void);


	// Accessor Methods
	/*------------------------------------------------------------------
	 getValue returns the value of the object.
	 ------------------------------------------------------------------*/
	int getValue(void);


	// Mutator Methods
	/*------------------------------------------------------------------
	 setValue sets the value of the object.
	 ------------------------------------------------------------------*/
	void setValue(int value);


private:

	// Object instance data
	int value;

};

#endif /* defined(__How_Fast_Can_You_Sort___Priority_Queue_Node__) */