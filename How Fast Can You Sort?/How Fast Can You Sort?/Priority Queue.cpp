/*------------------------------------------------------------------
 Priority Queue.cpp
 How Fast Can You Sort?

 Implementation of the priority queue (heap).

 Created by Neemias Freitas on 11/24/14.
 Copyright (c) 2014 Neemias Freitas. All rights reserved.
 ------------------------------------------------------------------*/


#include "Priority Queue.h"
using namespace std;


// Manager Methods (constructors & destructors)
/*------------------------------------------------------------------
 An optimized constructor for the priority queue.
 It takes one argument (n), which represents the number of random values to be inserted into the priority queue at the moment of its creation.
 ------------------------------------------------------------------*/
// INCORRECT HEAPFY - instead of 'n' inserts, should have done 'n/2' siftDowns
PQ::PQ(int n) {
	srand (int(time(NULL)));
	for (int i = 0; i < n; i++)
		this->insert(rand());
}

/*
 •	How is it possible to destroy a vector of PQNode's or PQNode *'s
 ------------------------------------------------------------------
 An optimized destructor for the priority queue.
 First, it destroys all PQNode's from the vector, then cleans the vector, and finally destroys the vector.
 ------------------------------------------------------------------
 PQ::~PQ(void) {
	for (vector<PQNode>::iterator it = this->heap.begin(); it < this->heap.end(); it++)
 it->~PQNode();
	this->heap.clear();
	this->heap.~vector();
 }
 */


// Accessor Methods
/*------------------------------------------------------------------
 findMax returns the highest priority value from the priority queue.
 This value is retrieved from the priority queue node.
 ------------------------------------------------------------------*/
int PQ::findMax(void) { return this->heap[0].getValue(); }

/*------------------------------------------------------------------
 count returns the number of values stored in the priority queue.
 ------------------------------------------------------------------*/
long PQ::count(void) { return this->heap.size(); }

/*------------------------------------------------------------------
 print displays the contents of the binary heap using a level-order traversal.
 ------------------------------------------------------------------*/
void PQ::print(void) {
	for (vector<PQNode>::iterator it = this->heap.begin(); it < this->heap.end(); it++)
		cout << it->getValue() << endl;
}


// Mutator Methods
/*------------------------------------------------------------------
 insert inserts a new value into the priority queue.
 Along with the insertion, a siftUp is performed.
 ------------------------------------------------------------------*/
void PQ::insert(int value) {
	this->heap.push_back(* new PQNode(value));
	this->siftUp();
}

/*------------------------------------------------------------------
 siftUp slides up the last element of the priority queue to its correct position according to the heap-order property.
 ------------------------------------------------------------------*/
void PQ::siftUp(void) {
	long hole = this->heap.size() - 1;
	int value = this->heap.back().getValue();
	for ( ; value > this->heap[hole/2.01].getValue(); hole /= 2.01) {  // for not recommended, since it does same as while
		int temp = this->heap[hole/2.01].getValue();
		this->heap[hole/2.01].setValue(this->heap[hole].getValue());
		this->heap[hole].setValue(temp);
	}
}

/*------------------------------------------------------------------
 deleteMax removes and returns the highest priority value from the priority queue, which is located at the root.
 Along with the deletion, a siftDown is performed.
 ------------------------------------------------------------------*/
int PQ::deleteMax(void) {
	if (this->heap.size()) {
		int deleted = this->heap[0].getValue();
		this->heap[0].setValue(this->heap.back().getValue());
		this->heap.back().~PQNode();
		this->heap.pop_back();
		this->siftDown();
		return deleted;
	}
	return false;
}

/*------------------------------------------------------------------
 siftDown slides down the first element of the priority queue to its correct position according to the heap-order property.
 ------------------------------------------------------------------*/
void PQ::siftDown(void) {
	int hole = 0;
	int child = 0;
	int value = this->heap[0].getValue();

	for ( ; value < max(this->heap[hole*2+1].getValue(), this->heap[hole*2+2].getValue()); hole = child) {  // two children
		int temp = max(this->heap[hole*2+1].getValue(), this->heap[hole*2+2].getValue());
		child = (this->heap[hole*2+1].getValue() == temp) ? hole*2+1 : hole*2+2;  // set new child
		this->heap[child].setValue(this->heap[hole].getValue());
		this->heap[hole].setValue(temp);

		// check if siftDown will go too far
		if (child*2+1 > this->heap.size()-1)  // no child
			break;
		else if (child*2+2 > this->heap.size()-1) {  // only one child
			if (value < this->heap[child*2+1].getValue()) {
				temp = this->heap[child*2+1].getValue();
				this->heap[child*2+1].setValue(this->heap[child].getValue());
				this->heap[child].setValue(temp);
			}
			break;
		}
	}
}

/*------------------------------------------------------------------
 heapSort sorts the priority queue in decreasing order.
 ------------------------------------------------------------------*/
// should have done in-place sort, not a copy of the queue...
void PQ::heapSort(void) {
	vector<PQNode> sortedQueue;
	while (this->heap.size()) { sortedQueue.push_back(* new PQNode(this->deleteMax())); }
	this->heap.swap(sortedQueue);
}