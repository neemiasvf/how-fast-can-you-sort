/*------------------------------------------------------------------
 Priority Queue Node.cpp
 How Fast Can You Sort?

 Implementation of the priority queue node as a class.

 Created by Neemias Freitas on 11/24/14.
 Copyright (c) 2014 Neemias Freitas. All rights reserved.
 ------------------------------------------------------------------*/


#include "Priority Queue Node.h"


// Manager Methods (constructors & destructors)
/*------------------------------------------------------------------
 An optimized constructor for the tree node.
 It takes one argument (value), which will set the value variable.
 ------------------------------------------------------------------*/
PQNode::PQNode(int value) { this->value = value; }

/*------------------------------------------------------------------
 An optimized destructor for the priority queue node.
 It sets NULL to the 'value' variable.
 ------------------------------------------------------------------*/
PQNode::~PQNode(void) { this->value = NULL; }


// Accessor Methods
/*------------------------------------------------------------------
 getValue returns the value of the object.
 ------------------------------------------------------------------*/
int PQNode::getValue(void) { return this->value; }


// Mutator Methods
/*------------------------------------------------------------------
 setValue sets the value of the object.
 ------------------------------------------------------------------*/
void PQNode::setValue(int value) { this->value = value; }