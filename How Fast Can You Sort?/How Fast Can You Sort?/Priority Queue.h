/*------------------------------------------------------------------
 Priority Queue.h
 How Fast Can You Sort?

 Definition of the priority queue (heap).
 In this priority queue, the elements with the highest priority are those which have the highest value.
 The priority queue is defined by a vector called heap containing priority queue nodes.

 Created by Neemias Freitas on 11/24/14.
 Copyright (c) 2014 Neemias Freitas. All rights reserved.
 ------------------------------------------------------------------*/


#ifndef __How_Fast_Can_You_Sort___Priority_Queue__
#define __How_Fast_Can_You_Sort___Priority_Queue__
#include <vector>
#include "Priority Queue Node.h"


class PQ {

public:

	// Manager Methods (constructors & destructors)
	/*------------------------------------------------------------------
	 An optimized constructor for the priority queue.
	 It takes one argument (n), which represents the number of random values to be inserted into the priority queue at the moment of its creation.
	 ------------------------------------------------------------------*/
	PQ(int n);

	/*
	 •	How is it possible to destroy a vector of PQNode's or PQNode *'s?
	 ------------------------------------------------------------------
	 An optimized destructor for the priority queue.
	 First, it destroys all PQNode's from the vector, then cleans the vector, and finally destroys the vector.
	 ------------------------------------------------------------------
	 ~PQ(void);
	 */


	// Accessor Methods
	/*------------------------------------------------------------------
	 findMax returns the highest priority value from the priority queue.
	 This value is retrieved from the priority queue node.
	 ------------------------------------------------------------------*/
	int findMax(void);

	/*------------------------------------------------------------------
	 count returns the number of values stored in the priority queue.
	 ------------------------------------------------------------------*/
	long count(void);

	/*------------------------------------------------------------------
	 print displays the contents of the binary heap using a level-order traversal.
	 ------------------------------------------------------------------*/
	void print(void);


	// Mutator Methods
	/*------------------------------------------------------------------
	 insert inserts a new value into the priority queue.
	 Along with the insertion, a siftUp is performed.
	 ------------------------------------------------------------------*/
	void insert(int value);

	/*------------------------------------------------------------------
	 deleteMax removes and returns the highest priority value from the priority queue, which is located at the root.
	 Along with the deletion, a siftDown is performed.
	 ------------------------------------------------------------------*/
	int deleteMax(void);

	/*------------------------------------------------------------------
	 heapSort sorts the priority queue in decreasing order.
	 ------------------------------------------------------------------*/
	void heapSort(void);


private:

	std::vector<PQNode> heap;  // Object instance data

	// Mutator Methods
	/*------------------------------------------------------------------
	 siftUp slides up the last element of the priority queue to its correct position according to the heap-order property.
	 ------------------------------------------------------------------*/
	void siftUp(void);

	/*------------------------------------------------------------------
	 siftDown slides down the first element of the priority queue to its correct position according to the heap-order property.
	 ------------------------------------------------------------------*/
	void siftDown(void);

};

#endif /* defined(__How_Fast_Can_You_Sort___Priority_Queue__) */