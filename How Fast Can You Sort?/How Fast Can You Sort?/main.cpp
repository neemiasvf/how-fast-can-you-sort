/*------------------------------------------------------------------
 main.cpp
 How Fast Can You Sort?

 A test driver for the 'How Fast Can You Sort?' project.

 Created by Neemias Freitas on 11/24/14.
 Copyright (c) 2014 Neemias Freitas. All rights reserved.
 ------------------------------------------------------------------*/


#include <iostream>
#include "Priority Queue.h"
using namespace std;


int main(int argc, const char * argv[]) {

	// Declare variables & instantiation
	int value = NULL;
	int option = NULL;
	PQ heap(1000000);  // Instantiation with 100 random numbers (makeHeap)

	// Main menu
	cout << "Welcome to Priority Queue system! (by Neemias Freitas)\
	\nThere are " << heap.count() << " number(s) already inserted in the heap. To start...";

	while (option != 7) {

		// Get user's option
		cout << "\nChoose an option:\
		\n1) Insert - insert a new value into the priority queue\
		\n2) Delete Max. - remove and print the highest priority value from the priority queue\
		\n3) Find Max. - print the highest priority value from the priority queue\
		\n4) Count - print the number of values stored in the priority queue\
		\n5) Print - print all the values in the priority queue using a level-order traversal\
		\n6) Heap Sort - perform a heap sort and print all the values in the priority queue in decreasing order\
		\n7) Quit - quit the program\
		\nOption: ";
		cin >> option;

		// Access/execute chosen option
		switch (option) {
			default:
				cout << "\nChoose a valid option!";
				break;

				// INSERT
			case 1: {
				cout << "\nEnter the value to be inserted: ";
				cin >> value;
				heap.insert(value);
				break;
			}

				// DELETE MAX.
			case 2:
				if (heap.count()) {
					cout << "\nValue removed from the priority queue: " << heap.deleteMax() << endl;
					break;
				}
				cout << "\nThe priority queue is empty! There is no value to be removed!\n";
				break;

				// FIND MAX
			case 3:
				if (heap.count()) {
					cout << "\nThe highest priority value from the priority queue is: " << heap.findMax() << endl;
					break;
				}
				cout << "\nThe priority queue is empty! There is no highest priority value!\n";
				break;

				// COUNT
			case 4:
				cout << "\nNumber of values stored in the priority queue: " << heap.count() << endl;
				break;

				// PRINT
			case 5:
				if (heap.count()) {
					heap.print();
					break;
				}
				cout << "\nThe priority queue is empty! There are no values to be printed!\n";
				break;

				// HEAP SORT
			case 6:
				if (heap.count()) {
					heap.heapSort();
					heap.print();
					break;
				}
				cout << "\nThe priority queue is empty! There are no values to be sorted or printed!\n";
				break;

				// QUIT
			case 7:
				break;
		};
	}
	return 0;
}